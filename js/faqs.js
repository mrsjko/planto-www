var faqs = {
    faqSections: [{
        title: "New to Planto?",
        questions: [{
                q: "What is Planto?",
                a: "Planto is a mobile application to help individuals manage their finances. We are in beta testing as of December 2017 with an aim to launch publicly in mid-January 2018. <p>Planto will be available for iOS and Android.</p>"
            }, 
            {
                q: "How does Planto work?",
                a: "Planto works by syncing with your existing bank and credit card accounts and showing you your financial standing in one single place. We currently support all the big banks in Hong Kong with support for more on the way."
            },
            {
                q: "What does ‘bank sync’ mean?",
                a: "Bank sync only authorizes Planto to securely retrieve your financial information at that point in time. This is a <b>read only</b> access to your account and money cannot be moved in or out. We only care about helping you managing your finances."
            }, 
            {
                q: "Is this a new concept?",
                a: "Not really! In the USA, UK, Japan, and Korea, personal finance management is a very common concept. Companies like <a class='blue' href='https://www.mint.com'>Mint.com</a> (more than 10m users), <a class='blue' href='https://www.ontrees.com/'>OnTrees</a> (millions of users), <a class='blue' href='https://moneyforward.com/'>MoneyForward</a> (more than 5m users). We wanted to be the first to bring this concept to Hong Kong. "
            },
            {
                q: "Is this legal?",
                a: "Yes. Engineers at Planto have built the same technology that powers some of the most successful personal finance management solutions in the US such as <a href='https://www.mint.com/'>Mint.com</a>. We are a fintech company that is simply disrupting the personal finance space and providing you more transparency and control over your finances."
            },
            {
                q: "Have banks like HSBC provided you with permission to integrate with them?",
                a: "Planto’s bank integration is dependent on your consent and not the bank. Since the financial information is yours, it is your decision to allow us access to your account and information."
            }, {
                q: "Do you also offer financial advice?",
                a: "Planto guides you towards financial wellness with insights. As of now, we offer you basic insights in form of expense analysis, help you understand if you have emergency savings and inform you of your saving behavior. We will continue to expand the range of insights to better guide you to manage your finances and reach your financial goals."
            }
        ]
    }, {
        title: "Security",
        questions: [{
                q: "Do you store my account balance and other data?",
                a: "Since we do not collect any information that can be used to identify you, your data is anonymous and cannot be linked to you in any way. <b>We do NOT store any credit card, account numbers, HKID or any address information.</b>"
            }, 
            {
                q: "How secure is the data?",
                a: "The anonymized data is stored in the cloud and is encrypted using industry standard encryption. The data as it comes from the server to your mobile device is also encrypted with the same encryption standards as used by banks.<p>We take security very seriously, and are always working hard to keep Planto and your data safe. The founders themselves use Planto daily and feel comfortable knowing that their data is secure!"
            },
            {
                q: "How is my data anonymized?",
                a: "At no point in the Planto journey do we ask for any information that can be used to identify you.<p>Example: when registering your Planto account, you're able to choose any username you want.</p>"
            },
            {
                q: "Do you save my banking username and password?",
                a: "Your credentials are never stored by Planto. Planto is a fintech company that has been designed to help you manage your expenses and money, and provide you better control of your finances."
            }, 
            {
                q: "Can someone else read my data?",
                a: "Absolutely not! The data is encrypted and anonymised so any financial information can never be traced back to any individual user."
            },
            {
                q: "Can I delete all my data with you?",
                a: "Yes! It is your data and you have the right to delete it. Simply go to your synced accounts and tap delete. This will delete all the information related to your account from our servers. Your information cannot be retrieved once deleted from Planto. <p>Want to get your transactions back? Just sync again!</p>"
            },
            {
                q: "What if my device is lost when I am logged into Planto?",
                a: "Planto requires you to login using a passcode or your fingerprint (if your phone supports it) everytime you access the app. In case you lose your phone, your data remains encrypted and can not be accessed by anyone."
            }
        ]
    }, {
        title: "Technology",
        questions: [{
            q: "How do you categorise my expenses?",
            a: "Categorisation of your transactions is tough, sometimes impossible, but the team has built a statistical model that breaks down your transaction description and checks it against all of the businesses in Hong Kong. Planto’s categorisation will get better with time as more people use the app and recategorize their transaction when it doesn’t look right."
        }, {
            q: "Will the app remind me to pay my bills?",
            a: "Planto’s alert center allows you to set alerts for credit card bills. With time, we will move on to suipport paying bills, rent and other recurring expenses to make it easy for you to manage your finances."
        }]
    }, {
        title: "Company",
        questions: [{
            q: "Who is behind Planto?",
            a: "Planto is founded by Computer Science alumni of HKU and HKUST who have worked for HSBC's Technology and Retail Bank teams as well as MoneyHero. Taha, Ankit and Yousaf are behind Planto. Say hello to us at <a href='mailto:hello@planto.io'>hello@planto.io</a>!"
        }, {
            q: "Is this startup here to sell our data and make money off of it?",
            a: "Planto was initially made for the personal use of the founders since they wanted to solve the problem of having one view into their finances. When our friends saw the app, they wanted in too! So we decided to work on this full-time and give access to Planto to a lot more people. <p>Planto is here to help you improve <b>your</b> financial well-being, not the well-being of people who might be interested in your financial data. We will <b>never</b> sell your data to <b>anyone</b> or use it in any way without your prior consent</p>"
        }, {
          q: "How do you make money?",
          a: "Planto will start to recommend you offers and (non-investment) financial products that suit your financial profile and get you access to cheaper and better financial services. If you decide to follow up on our recommendation, we get a small share from the provider of the product. We never provide your information to the provider until <b>you want us to</b>."
        }]
    }]
}

Handlebars.registerHelper('genUniqueId', function(title, question) {
    var title = Handlebars.escapeExpression(title),
        q = Handlebars.escapeExpression(question);

    var idString = title + q;
    return idString.toCamelCase();
});

var source = $("#faq-template").html();
var template = Handlebars.compile(source);
var html = template(faqs);

$("#faq-insert").replaceWith(html);