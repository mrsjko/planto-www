function onFormSubmit(token) {
    $.ajax({
        url: 'https://core.planto.io/emailSubscribe',
        type: 'post',
        data: {
            "email": $("#sub-email").val(),
            "g-recaptcha-response": token
        },
        success: function() {
            $("#subscribe-form").replaceWith("<div class='carousel-bottom-secondary'>We will send you an email when Planto is ready for download!</div>");
            $("#subscribe-form-title").hide();
        },
        error: function() {
        	grecaptcha.reset();
        }
    });
}